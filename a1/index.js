console.log("Hello World");
///////////////////////////////////////////////////////////////////////

/*Part 1*/
let trainer1 = {
	name: "Ash Ketchum",
	age: 18,
	pokemon: [
		"Pikachu",
		"Squirtle",
		"Charmander"
	],
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	talk: function(){
		console.log("Pikachu! I choose you!")
	}
};
console.log(trainer1);

console.log("Result of dot notation: ");
console.log(trainer1.name);

console.log("Result of square bracket notation: ");
console.log(trainer1.pokemon);

console.log("Result of talk method: ");
trainer1.talk();
///////////////////////////////////////////////////////////////////////
/*Part 2*/

function Pokemon(name, level, health, attack){
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level * 1.5;
	this.tackle = function(target){
		console.log(this.name+" tackled "+target.name);
		target.health = target.health - this.attack;
		console.log(target.name+" health is now reduced to "+target.health);
		if(target.health <= 0){
			console.log(target.faint());
		}
	}
	this.faint = function(){
		console.log(this.name+" fainted");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let newtwo = new Pokemon("Newtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(newtwo);
///////////////////////////////////////////////////////////////////////
/*Part 3*/

/*	this.tackle = function(target){
		console.log(this.name+" tackled "+target.name);
		target.health = target.health - this.attack;
		console.log(target.name+" health is now reduced to "+target);
		if(target.health <= 0){
			target.faint()
		}
	this.faint = function(){
		console.log(this.name+" fainted");
	}
	}*/
