console.log("hello");

/*
	elements in an array can only be access by indexes.

	an object is similat to an array.
	object uses properties.

	in object, we can easily give a label to each value.
*/


//Object
/*
	- An Object is a data type that is used to represent real world objects
	- it is a collertion of related data and/or functionalities.
	- information stored in objects are represented in a "key:value" pair.
		"key" - "property" of an object
		"value" - actual data to be stored

	- different data type may be stored in an object's property creating complex data structures.
*/


//2 ways in creating objects in JS
	/*
		1. Object Literal Notation
			(let object = {})
		
		2. Object Constructor Notation
			Object Instantation (let object = new Object())
	*/


	//Object Literal Notation
	/*
		- creating objects using initializer or literal notation.
		- camelCase

		Syntax:
			let objectName = {
				keyA : valueA,
				keyB : valueB
			}
	*/
	let cellphone = {
		name: "Nokia 3210",
		manufactureDate: 1999
	};
	console.log("Result from creating objects using literal notation: ");
	console.log(cellphone);


	//Mini Activity
	/*
		create a cellphone2 tulad ng nasa taas.
	*/
	let cellphone2 = {
		name: "Motorola",
		manufactureDate: 2000
	}
	console.log(cellphone2);


	let ninja = {
		name: "Naruto Uzumaki",
		village: "Konoha",
		children: ["Boruto", "Himawari"]
	}
	console.log(ninja);


	//Object Constructor Notation
	/*
		creating Objects using a Contrustor Notation

		creates a reusable "function" to create several objects that have the same data structure

		Syntax:
			function objectName(keyA, keyB){
				this.keyA = keyA;
				this.keyB = keyB;
			}
	*/

	//"this" keyword refers to the properties within the object.
	//it allows the assignment of new object's properties.
	//by associating them with values received from the constructor function's parameter (para stang template o blue print)
	function Laptop(name, manufactureDate){
		this.name = name;
		this.manufactureDate = manufactureDate;
	}
	//create an instance object using the Laptop constructor.
	let laptop = new Laptop("Lenovo", 2008);
	console.log("Result from creating objects using object constructor.");
	console.log(laptop);

	//the "new" operator creates an instance of an object (new object)
	let myLaptop = new Laptop("MacBook Air", 2020);
	console.log("Result from constructor.");
	console.log(myLaptop);


	//Mini Activity
	/*
		create 3 new instance object of a loptop
	*/
	let computer1 = new Laptop("Notepad", 1999);
	console.log(computer1);

	let computer2 = new Laptop("Acer", 2021);
	console.log(computer2);

	let computer3 = new Laptop("HP", 2022);
	console.log(computer3);


	let oldLaptop = Laptop("Portal", 1980);
	console.log("Result walang (new) ");
	console.log(oldLaptop);	//return, undefine.
	//ang "new" ay parang "return" ng function.

	//Create Empty Object
	let computer = {};
	let myComputer = new Object();
	console.log(computer);
	console.log(myComputer);
	//this is how to create an empty objects.


	//Accessing Object Properties.
	/*
		1. using dot notation
	*/
	console.log("Result from dot notation: "+myLaptop.name);
	//highly recommend to use dot notation.

	/*
		2. square bracket notation.
	*/
	console.log("Result from square bracket notation: "+myLaptop["name"]);
	//accessing object properties using the square bracket notation and array indexes can cause confusion.

	//Accessing Array of Objects
	let arrayObj = [laptop, myLaptop];
	/*we may be confused for accessing array indexes*/
	console.log(arrayObj[0]["name"]);
	/*This tells us that array[0] is an object by using the dot notation.*/
	console.log(arrayObj[0].name);



//Initializing, Adding, Deleting, Reassigning, Object Properties.
	//create an object using object literals.
	let car = {};
	console.log("current value of car object: ");
	console.log(car);	//{}

	//Initializing or Adding properties
	car.name = "Honda Civic";
	console.log("result from adding property using dot notation.");
	console.log(car);

	//Initializing or Adding properties Using Bracket Notation (not recommended).
	console.log("result from adding property using square bracket notation.");
	car["manufacture date"] = 2019;
	console.log(car);

	//Deleting Object Properties
	delete car["manufacture date"];
	console.log("result from deleting properties.");
	console.log(car);

	car["manufactureDate"] = 2019;
	console.log(car);


	//Reassign Object Property Values
	car.name = "Toyota Vios";
	console.log("Result from reassigning property values");
	console.log(car);

	delete car.manufactureDate;	//delete a property
	console.log(car);


//Object Methods
	/*
		a mthod is a function which  is a property of an object
	*/
	let person = {
		name: "John",
		talk: function(){
			console.log("Hello my name is "+ this.name);
		}
	};
	console.log(person);
	console.log("Result from object methods: ");
	person.talk();	//this is how to invoke a function inside the object. puwede rin sa console mag invoke.

/*	person.walk = function(){
		console.log(this.name + " walked " + 500 + " steps forward");
	};
	console.log(person);*/

	person.walk = function(steps){
		console.log(this.name + " walked " + steps + " steps forward");
	};
	console.log(person);
	person.walk(50);

	/*
		Methods are useful for creating reusable functions that perform tasks related to objects.
	*/

	let friend = {
		firstname: "Joe",
		lastname: "Smith",
		address: {
			city: "Austin",
			country: "Texas"
		},
		emails: ["joes@mail.com", "joesmith@mail.xyz"],
		
		introduce: function(){
			console.log("Hello my name is " + this.firstname + " " + " I line in " + this.address.city + ", " + this.address.country);
		}
	};
	friend.introduce();


//Real World Application of Objects
	/*
		scenario:
		1. we would like to create a game that would have several Pokemon interact with each other.
		2. Every Pokemon would have the same set of stats, properties, and function.

			stats:
				name:
				level:
				health: level*2
				attack: level
	*/
	//create an object constructor to lessen the process in creating the pokemon.

	function Pokemon(name, level){
		//properties
		this.name = name;
		this.level = level;
		this.health = level * 2;
		this.attack = level;
		this.tackle = 
		function(target){
	console.log(this.name + " tackled " + target.name);
	target.health = target.health - this.attack

	console.log(target.name + " health is now reduced to " + target)

			if(target.health <= 0){
					target.faint()
				}
				this.faint = function(){
					console.log(this.name + " fainted");
				};
		}
	};

	let pikachu = new Pokemon ("Pikachu", 88);
	console.log(pikachu);

	let rattata = new Pokemon ("Ratata", 10);
	console.log(rattata);


//Mini Activity
	/*
		- if the target health is less than or equal to 0 we will invoke the faint(method), otherwise printout the pokemen's new health			example if health is not less than 0
		//rattata's health is now reduced to 5

		-reduces the target object's health property by subtracting and reassigning it's value based on the pokemon's attack.
	*/
			/*1. target.health = target.health - this.attack*/







	/*function Pokemon(name, level, health){
		this.name = name;
		this.level = level;
		this.health = health;
		this.attack = level;

		this.tackle = function(){
			console.log(this.name + " tackled " +target.name);
			console.log(target.name + "'s health is now reduce to "+ (target.health - this.attack));
		},

		this.faint = function(){
			console.log(this.name + " fainted");
		};

	}
	let squirtle = new Pokemon("Squirtle", 99, 200);
	let snorlax = new Pokemon("Snorlax", 75, 500);

	console.log(squirtle);
	console.log(snorlax);*/